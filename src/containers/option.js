import React, { Component } from 'react';
import data from '../data';
import assetsPath from '../const';

export default class extends Component {
  render() {

    const {
      question,
      selectedOption,
      prevButtonHandler,
      nextButtonHandler,
      closeButtonHandler,
      checkAnswerButtonHandler
    } = this.props;

    const options = data[question]['options']; 
    const groom = options[selectedOption];

    let buttonText = "Выбрать этого жениха";
    if (question === 1) buttonText = "Выбрать эту невесту";

    return (
      <div className="option">
        
        <button className={"left"} onClick={prevButtonHandler} >&larr;</button>
        <button className={"right"} onClick={nextButtonHandler}>&rarr;</button>
        <button className="close" onClick={closeButtonHandler}>&times;</button>

        <div className="option__photo">
          <div className="option__photo__border">
            {
              groom.image ?
              <img src={`${assetsPath}/assets/Grooms/Portraits/${groom.image}`} width="99" /> :
              <img src={`${assetsPath}/assets/bride-1.png`} width="99" />
            }
          </div>
        </div>

        <div className="option__name" dangerouslySetInnerHTML={{__html: groom.name}} />
        <div className="option__country" dangerouslySetInnerHTML={{__html: groom.country}} />

        <dl>
            {
              groom.info.map((value, key) => {
                return (
                  <div key={key}>
                    <dt dangerouslySetInnerHTML={{__html: value[0]}} />
                    <dd dangerouslySetInnerHTML={{__html: value[1]}} />
                  </div>
                )
              })
            }
        </dl>

        <div className="option__text">
          {
            groom.text.map((value, key) => {
              return (
                <p key={key} dangerouslySetInnerHTML={{__html: value }} />
              )
            })
          }
        </div>

        <button className="check-answer" onClick={checkAnswerButtonHandler}>{buttonText}</button>

      </div>
    )
  }
}