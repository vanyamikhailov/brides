export IntroScreen from './intro';
export QuizScreen from './quiz';
export OptionScreen from './option';
export AnswerScreen from './answer';
export ResultScreen from './result';