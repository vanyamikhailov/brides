import React from 'react';
import data from '../data';
import assetsPath from '../const';

export default class extends React.Component {

  render() {

    const { question, option, correct, nextButtonHandler } = this.props;


    return (
      <div className="answer">
        <div className="answer__icon">
          <hr />
          <div className="answer__img" style={{width: (correct ? 57 : 40)}}>
            {
              correct ?
              <img src={`${assetsPath}/assets/success.png`} width="57" /> :
              <img src={`${assetsPath}/assets/error.png`} width="40" />
            }
          </div>
        </div>

        <header dangerouslySetInnerHTML={{__html: data[question].options[option].output[0] }} />

        <div className="answer__text">
          {
            data[question].options[option].output.map((value, key) => {
              if (key === 0) return null

              return (
                <p key={key} dangerouslySetInnerHTML={{__html: value }} />
              )
            })
          }
        </div>

        <button onClick={nextButtonHandler}>Продолжить</button>

      </div>
    )


  }


}