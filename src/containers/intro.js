import React, { Component } from 'react';

export default class extends Component {
  render() {
    const { startButtonHandler, dynastyHandler } = this.props;

    return (
      <div className="intro">


        <h1>Как правильно заключить династический брак</h1>

        <div className="description">
          Найдите супругов детям и&nbsp;племянницам Петра&nbsp;I, руководствуясь интересами империи. В&nbsp;конце вы&nbsp;получите идеальную шпаргалку по&nbsp;эпохе дворцовых переворотов
        </div>

        <div className="credit">
          <em>Подготовили</em> <a href="https://arzamas.academy/authors/309" target="_blank">Наталья Володина</a>, <a target="_blank" href="https://arzamas.academy/authors/307">Николай Асламов</a>
        </div>

        <div className="whatshappened" onClick={dynastyHandler}>Что происходит?</div>

        <header className="rules">Правила</header>

        <ol>
          <li>Задача игрока&nbsp;&mdash; устроить семейную жизнь одного сына, двух дочерей и&nbsp;двух племянниц Петра&nbsp;I.</li>

          <li>
              В&nbsp;каждом из&nbsp;пяти раундов игрок получает:
              <ul>
                <li>анкету жениха или невесты из&nbsp;семьи Романовых;</li>
                <li>описание международной ситуации в&nbsp;тот год, когда происходило сватовство;</li>
                <li>анкеты трех претендентов.</li>
              </ul>
          </li>

          <li>Правильным выбором в&nbsp;игре считается тот выбор, который был сделан в&nbsp;действительности. Имейте в&nbsp;виду, что решения практически всегда принимал сам Петр, исходя из&nbsp;своих представлений о&nbsp;целесообразности. Исключение составляет только последняя невеста&nbsp;&mdash; дочь Петра&nbsp;I Елизавета, которая была обручена уже после смерти отца.</li>

          <li>Завершив последний раунд, игрок сможет увидеть, как выбор женихов и&nbsp;невест предопределял историю Российской империи до&nbsp;конца XVIII&nbsp;века.</li>
        </ol>

        <button onClick={startButtonHandler}>Играть</button>

      </div>
    )
  }
}