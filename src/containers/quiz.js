import React, { Component } from 'react';
import data from '../data';
import assetsPath from '../const';

export default class extends Component {

  render() {
    const { question, pinClickHandler } = this.props;

    const questionData = data[question];

    let sidebarClassName = 'map__sidebar';
    if (question === 3 || question === 4) sidebarClassName += ' right';

    return (
      <div className="quiz">

        <p className="subheader">Петр I. Геополитика</p>
        <header>Как отдать всех дочерей в рабство и не облажаться</header>

        <hr className="small" />

        <div className="pagination">{question + 1}/{data.length}</div>

        <div className="bio">

          <div className="bio__photo">
            <hr />

            <div className="photo-border">
              <img src={`${assetsPath}/assets/Brides/${questionData.image}`} width="99" />
            </div>
          </div>

          <div className="bio__name">{questionData.name}</div>
          <div className="bio__year">{questionData.year} год</div>
          <div className="bio__desc" dangerouslySetInnerHTML={{__html: questionData.description }} />

          <hr className="small" />

          <dl>
            {
              questionData.info.map((value, key) => {
                return (
                  <div key={key}>
                    <dt dangerouslySetInnerHTML={{__html: value[0]}} />
                    <dd dangerouslySetInnerHTML={{__html: value[1]}} />
                  </div>
                )
              })
            }
          </dl>

          {
            questionData.quote ?
            <blockquote>
              <div dangerouslySetInnerHTML={{__html: questionData.quote.text }} />
              <footer>
                <img src={`${assetsPath}/assets/bride-1.png`} width="45" />
                <p dangerouslySetInnerHTML={{__html: questionData.quote.author }} />
                {
                  questionData.quote.description ?
                  <p dangerouslySetInnerHTML={{__html: questionData.quote.description }} /> :
                  null
                }
              </footer>
            </blockquote> :
            null
          }

        </div>

        <div className="map">

          <header>Ситуация</header>

          <div className="map__image">
            <div className="map__inner">

              <span className="map__label" style={{top: 152, left: 838, width: 90}}>Российская империя</span>
              <span className="map__label" style={{top: 274, left: 703, width: 60}}>Речь Посполитая</span>
              <span className="map__label" style={{top: 50, left: 506}}>Швеция</span>
              <span className="map__label" style={{top: 162, left: 427}}>Дания</span>
              <span className="map__label" style={{top: 272, left: 420, fontSize: 9}}>Ганновер</span>
              <span className="map__label" style={{top: 254, left: 466, fontSize: 9}}>Мекленбург</span>
              <span className="map__label" style={{top: 286, left: 503}}>Пруссия</span>
              <span className="map__label" style={{top: 324, left: 489}}>Саксония</span>
              <span className="map__label" style={{top: 350, left: 412, width: 70}}>Священная Римская империя</span>
              <span className="map__label" style={{top: 469, left: 389}}>Швейцария</span>
              <span className="map__label" style={{top: 530, left: 367}}>Савойя</span>
              <span className="map__label" style={{top: 410, left: 484}}>Бавария</span>
              <span className="map__label" style={{top: 503, left: 470}}>Венеция</span>
              <span className="map__label" style={{top: 577, left: 469, fontSize: 9}}>Тоскана</span>
              <span className="map__label" style={{top: 645, left: 560}}>Неаполь</span>
              <span className="map__label" style={{top: 435, left: 619}}>Австрия</span>
              <span className="map__label" style={{top: 555, left: 710, width: 100}}>Османская империя</span>
              <span className="map__label" style={{top: 278, left: 193}}>Великобритания</span>
              <span className="map__label" style={{top: 463, left: 266}}>Франция</span>
              <span className="map__label" style={{top: 620, left: 110}}>Испания</span>

              {
                question === 4 ?
                <div className="map__label" style={{top: 39, left: -11, width: 371, height: 371}}>
                  <img src={`${assetsPath}/assets/persia.png`} width="371" />
                  <span className="map__label" style={{top: 204, left: 18, width: 60}}>Османская империя</span>
                  <span className="map__label" style={{top: 192, left: 171}}>Персия</span>
                </div> :
                null
              }

              <div className={sidebarClassName}>
                <div className="sidebar__header">
                  <hr />
                  <span>{questionData.year} год</span>
                </div>

                <div className="sidebar__text">
                  {
                    questionData.context.map((value, key) => {
                      return <p key={key} dangerouslySetInnerHTML={{__html: value}} />;
                    })
                  }
                </div>
              </div>

              {
                questionData.options.map((value, key) => {
                  return <button
                    key={key}
                    className="pin"
                    style={{top: value.pin.y, left: value.pin.x}}
                    onClick={pinClickHandler.bind(this, key)}
                  >
                    <img className="line" src={`${assetsPath}/assets/pinline.png`} width="9" />
                    <div className="pinhead">
                      {
                        value.image ?
                        <img src={`${assetsPath}/assets/Grooms/Portraits/${value.image}`} width="43" /> :
                        null
                      }
                    </div>
                  </button>
                })
              }

            </div>
          </div>



        </div>
         
      </div>
    )
  }

}

