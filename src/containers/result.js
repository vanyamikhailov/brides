import React from 'react';
import $ from 'jquery';

import { TimelineMax } from 'gsap';
import data, { results, slides } from '../data';
import assetsPath from '../const';
import { shareToFacebook, shareToVK, shareToOK, shareToTwitter } from '../utils';


class Share extends React.Component {

  // state = {

  // }

  shareToFacebook() {
    shareToFacebook('http://arzamas.academy')
  }

  shareToTwitter() {
    shareToTwitter('http://arzamas.academy', 'Как отдать всех дочерей в рабство и не облажаться')
  }

  shareToOK() {
    shareToOK('http://arzamas.academy')
  }

  shareToVK() {
    shareToVK('http://arzamas.academy')
  }

  render() {

    const { answers } = this.props;
    const correctAnswers = answers.filter((v,k) => v).length;
    const result = results[correctAnswers];

    return (
      <div className="share">
        <div className="portraits">
          {
            answers.map((value, key) => {
              if (value === true) {
                return (
                  <div key={key}>
                    <img src={`${assetsPath}/assets/Brides/${data[key].image}`} width="60" />
                  </div>
                )
              }
            })
          }
        </div>

        <header dangerouslySetInnerHTML={{__html: result.header}} />
        <div className="subheader" dangerouslySetInnerHTML={{__html: result.subheader }} />

        <hr />

        <p>Поделиться в соцсети:</p>

        <ul>
          <li>
            <button onClick={this.shareToVK.bind(this)}>
              <img src={`${assetsPath}/assets/vk.png`} width="16" />
            </button>
          </li>
          <li>
            <button onClick={this.shareToFacebook.bind(this)}>
              <img src={`${assetsPath}/assets/facebook.png`} width="10" />
            </button>
          </li>
          <li>
            <button onClick={this.shareToOK.bind(this)}>
              <img src={`${assetsPath}/assets/ok.png`} width="10" />
            </button>
          </li>
          <li>
            <button onClick={this.shareToTwitter.bind(this)}>
              <img src={`${assetsPath}/assets/twitter.png`} width="17" />
            </button>
          </li>
        </ul>

      </div>
    )
  }
}

class Dynasty extends React.Component {

  state = {
    slide: 0,
    timelineClass: null
  }

  initTimeline() {
    const tl = new TimelineMax({paused: true});

    this.tl = tl;
    window.tl = tl;

    // hack
    tl.to('vl-1', 0, {}, 'slide-0')
    // tl.to('#scheme', 0, {'height': 560}, 'slide-0')

    tl.to('#hl-5', 0.25, {'width': 40}, 'slide-1')
      .to('#fridrikh', 0.4, {'opacity': 1}, 'slide-1')
    // tl.to('#scheme', 0.4, {'height': 560}, 'slide-0')

    tl.to('#hl-6', 0.25, {'width': 60}, 'slide-2')
    tl.to('#sks', 0.4, {'opacity':1.2}, 'slide-2')
    // tl.to('#fridrikh .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-2')
    // tl.to('#scheme', 0.4, {'height': 560}, 'slide-2')

    tl.to('#vl-8', 0.25, {'height': 172}, 'slide-3')
    tl.to('#petr-ii', 0.4, {'opacity':1}, 'slide-3')
    // tl.to('#sks .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-3')
    // tl.to('#scheme', 0.4, {'height': 1010}, 'slide-3')

    tl.to('#klmsh', 0.25, {'opacity': 1}, 'slide-4')
    tl.to('#hl-7', 0.4, {'width': 40}, 'slide-4')

    // tl.to('#alexey-p .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-5')
    tl.to('#vl-13', 0.25, {'height': 363}, 'slide-5')
    tl.to('#anna-l', 0.4, {'opacity':1}, 'slide-5')

    // tl.to('#petr-i .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-6')
    tl.to('#crown', 0.4, {'left': 513}, 'slide-6')
    tl.to('#karl-f', 0.4, {'opacity': 1}, 'slide-6')
    tl.to('#hl-8', 0.4, {'width': 40}, 'slide-6')
    // tl.to('#praskovia .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-6')

    // tl.to('#ekaterina-am .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-7')
    tl.to('#crown', 0.4, {'top': 613, 'left': 896}, 'slide-7')
    tl.to('#hl-9', 0.4, {'width': 42 }, 'slide-7')
    tl.to('#karl-a', 0.4, {'opacity': 1}, 'slide-7')

    tl.to('#petr-iii', 0.4, {'opacity': 1}, 'slide-8')
    tl.to('#vl-11', 0.25, {'height': 172}, 'slide-8')
    // tl.to('#anna-p .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-8')


    // tl.to('#petr-ii .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-9')
    tl.to('#crown', 0.4, {'top': 223, 'left': 238}, 'slide-9')


    tl.to('#anton', 0.4, {'opacity': 1}, 'slide-10')
    tl.to('#hl-12', 0.4, {'width': 40 }, 'slide-10')
    // tl.to('#evdokia .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-10')
    // tl.to('#ekaterina-i .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-10')
    // tl.to('#karl-f .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-10')

    // tl.to('#anna-i .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-11')
    tl.to('#ivan-vi', 0.4, {'opacity': 1}, 'slide-11')
    tl.to('#vl-14', 0.25, {'height': 130}, 'slide-11')
    tl.to('#crown', 0.4, {'top': 773, 'left': 142}, 'slide-11')

    tl.to('#crown', 0.4, {'top': 413, 'left': 543}, 'slide-12')
    
    tl.to('#vl-9', 0.25, {'opacity': 1}, 'slide-13')
    tl.to('#vl-10', 0.25, {'opacity': 1}, 'slide-13')
    tl.to('#hl-10', 0.25, {'opacity': 1}, 'slide-13')
    tl.to('#ioganna', 0.4, {'opacity': 1}, 'slide-13')

    tl.to('#chirstian', 0.4, {'opacity': 1, delay: 0.4}, 'slide-13')
    tl.to('#hl-11', 0.4, {'width': 40, delay: 0.4 }, 'slide-13')

    tl.to('#vl-12', 0.25, {'height': 172, delay: 0.8}, 'slide-13')
    tl.to('#ekaterina-ii', 0.4, {'opacity': 1, delay: 0.8}, 'slide-13')
    tl.to('#hl-13', 0.25, {'width': 339, delay: 0.9 }, 'slide-13')
    
    // tl.to('vl-1', 0, {}, 'slide-14')
    // tl.to('#anna-l .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-14')

    tl.to('#vl-15', 0.25, {'height': 130 }, 'slide-14')
    tl.to('#pavel-i', 0.4, {'opacity': 1, delay: 0.2}, 'slide-14')
    tl.to('#chirstian .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-14')

    // tl.to('#elizabeth-p .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-16')
    // tl.to('#klmsh .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-16')
    tl.to('#ioganna .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-15')
    tl.to('#crown', 0.4, {'top': 614, 'left': 690}, 'slide-15')

    // tl.to('#petr-iii .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-17')
    tl.to('#crown', 0.4, {'top': 614, 'left': 295}, 'slide-16')
    
    // tl.to('vl-1', 0, {}, 'slide-17')
    // tl.to('#ivan-vi .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-18')

    // tl.to('#anton .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-19')
    // tl.to('#ekaterina-ii .photo', 0.4, {'backgroundColor': '#232323'}, 'slide-19')
    tl.to('#crown', 0.4, {'top': 773, 'left': 499}, 'slide-17')

    // hack
    tl.to('vl-1', 0, {}, 'slide-18')
  }

  // constructor(){
  //   super()
  // }

  scrollHandler() {
    const lastScrollTop = $(window).scrollTop();
    const dynastyOffset = $('.dynasty').offset().top;
    const dynastyEndOffset = $('.dynasty__end').offset().top;

    // console.log('dynastyOffset', dynastyOffset);
    // console.log('dynastyEndOffset', dynastyEndOffset);
    // console.log('lastScrollTop', lastScrollTop);

    // if (dynastyEndOffset > 0) {
    if (lastScrollTop > dynastyEndOffset - $(window).height()) {
      this.setState({
        timelineClass: 'visible static'
      })
    } else {
      if (lastScrollTop > dynastyOffset - 550) {
        this.setState({
          timelineClass: 'visible'
        })
      } else {
        this.setState({
          timelineClass: null
        })
      }
    }
  }

  scrollToRow(row, callback=$.noop) {

    const scrollAnimationTime = 350
    const callbackTimeout = 100

    const bigScreen = $(window).height() > 740 && row !== 0;
    if (bigScreen) {
      row = row - 1
    } 

    const superBigScreen = $(window).height() > 950;
    if (superBigScreen) {
      row = 0
    } 

    const TOP_OFFSET = 50;

    if (row === 0) {
        $('html, body').animate({
          scrollTop: $(".dynasty__scheme").offset().top + 30 - TOP_OFFSET
        }, scrollAnimationTime, () => {
          setTimeout(callback, callbackTimeout)
        });
    }

    if (row === 1) {
        $('html, body').animate({
          scrollTop: $(".dynasty__scheme").offset().top + 205 - TOP_OFFSET
        }, scrollAnimationTime, () => {
          setTimeout(callback, callbackTimeout)
        });
    }

    if (row === 2) {
        $('html, body').animate({
          scrollTop: $(".dynasty__scheme").offset().top + 405 - TOP_OFFSET
        }, scrollAnimationTime, () => {
          setTimeout(callback, callbackTimeout)
        });
    }
  }

  componentDidMount() {
    window.$ = $
    $(window).bind('scroll', this.scrollHandler.bind(this))
    this.initTimeline.bind(this)()
  }

  componentWillUnmount() {
    $(window).unbind('scroll', this.scrollHandler)
  }

  componentDidUpdate(prevProps, prevState) {

    const { slide } = this.state;

    function animate(slide, prevState) {
      if (slide > prevState.slide) {
        console.log(`slide-${slide}`, `slide-${slide+1}`)
        this.tl.tweenFromTo(`slide-${slide}`, `slide-${slide + 1}`)
      } else {
        console.log(`slide-${slide + 2}`, `slide-${slide + 1}`)
        this.tl.tweenFromTo(`slide-${slide + 2}`, `slide-${slide + 1}`)
      }
    }

    if (slide !== prevState.slide) {

      if (slides[slide].year === 1711 && slides[prevState.slide].year === 1715) {
        this.scrollToRow(1, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1715) {
        this.scrollToRow(2, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1716) {
        this.scrollToRow(1, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1718) {
        this.scrollToRow(2, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1725) {
        this.scrollToRow(0, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1727) {
        this.scrollToRow(2, animate.bind(this, slide, prevState))
      } else if (slides[slide].year === 1740) {
        this.scrollToRow(2, animate.bind(this, slide, prevState))
      } else {
        animate.bind(this, slide, prevState)()
      }

    }

  }

  prevSlide() {
    const { slide } = this.state;

    const prevSlide = slide - 1;
    if (prevSlide < 0) return;

    this.setState({
      slide: prevSlide
    })

  }

  nextSlide() {
    const { slide } = this.state;

    const nextSlide = slide + 1;
    if (nextSlide >= slides.length) return;

    this.setState({
      slide: nextSlide
    })
  }


  render() {

    const { slide, timelineClass  } = this.state;

    const timelineCurrentYear = slides[slide].year;
    let timelineCurrentWidth = null;

    let initialOffset = 130;
    let widthPerYear = 13;
    let yearsDiff = timelineCurrentYear - 1710;

    if (timelineCurrentYear === 1796) {
      timelineCurrentWidth = 749;
    } else {
      if (timelineCurrentYear > 1530) {
        initialOffset = 3 * 130;
        widthPerYear = 6.5;
        yearsDiff = timelineCurrentYear - 1730;
      }

      timelineCurrentWidth = yearsDiff * widthPerYear + initialOffset;
    }

    let timelineClassName = 'timeline';
    if (timelineClass !== null) timelineClassName += ` ${timelineClass}`

    let prevButtonClass = 'prev';
    if (slide - 1 < 0) prevButtonClass += ' disabled'

    let nextButtonClass = 'next';
    if (slide + 1 >= slides.length) nextButtonClass += ' disabled';

    return (
      <div className="dynasty">

        <header>
          <hr />
          <span>
            Генеалогическое древо Романовых
          </span>
        </header>

        <p className="subheader">В&nbsp;1722 году Петр I&nbsp;принял закон о&nbsp;престолонаследии, согласно которому унаследовать престол мог любой человек, которого назначит государь. Вскоре после этого он&nbsp;умер, так и&nbsp;не&nbsp;выбрав себе наследника; императрицей стала его жена Екатерина I. После ее&nbsp;смерти на&nbsp;протяжении тридцати пяти лет к&nbsp;власти поочередно приходили члены всех ветвей семьи Романовых, появившихся в&nbsp;результате браков детей и&nbsp;племянниц Петра Великого.</p>

        <hr />


        <div className="dynasty__scheme" id="scheme">

          <div className="crown" id="crown">
            <img src={`${assetsPath}/assets/korona.png`} width="25" />
          </div>

          <div className="vl" id="vl-1" />
          <div className="vl" id="vl-2" />
          <div className="vl" id="vl-3" />

          <div className="vl" id="vl-4" />
          <div className="vl" id="vl-5" />

          <div className="vl" id="vl-6" />
          <div className="vl" id="vl-7" />
          <div className="vl petr-ii" id="vl-8" />

          <div className="vl" id="vl-9" />
          <div className="vl" id="vl-10" />

          <div className="vl" id="vl-11" />
          <div className="vl" id="vl-12" />
          <div className="vl anna-l" id="vl-13" />

          <div className="vl" id="vl-14" />
          <div className="vl" id="vl-15" />



          <div className="hl" id="hl-1" />
          <div className="hl" id="hl-2" />
          <div className="hl" id="hl-3" />

          <div className="hl" id="hl-4" />

          <div className="hl fridrikh" id="hl-5" />
          <div className="hl sks" id="hl-6" />
          <div className="hl klmsh" id="hl-7" />

          <div className="hl karl-f" id="hl-8" />

          <div className="hl" id="hl-9" />

          <div className="hl" id="hl-10" />

          <div className="hl" id="hl-11" />

          <div className="hl" id="hl-12" />
          <div className="hl" id="hl-13" />



          {/* row 1*/}

          <div className="person dead" id="ivan-v">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/1.png`} width="58" />
            </div>
            <div className="name">Иван V</div>
            <div className="year">1666 - 1696</div>
          </div>

          <div className={(slide > 5) ? "person dead" : "person"} id="praskovia">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/2.png`} width="58" />
            </div>
            <div className="name">Прасковья (Салтыкова)</div>
            <div className="year">
              { 
                (slide > 5) ?
                "1664 - 1723" :
                "р. 1664"  
              }
            </div>
          </div>

          <div className={(slide > 6) ? "person dead" : "person"} id="ekaterina-am">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/3.png`} width="58" />
            </div>
            <div className="name">
             { 
                (slide > 5) ?
                "Екатерина I" :
                "Екатерина (Марта Скавронская)"
              }              
            </div>
            <div className="year">
              { 
                (slide > 6) ?
                "1684 - 1727" :
                "р. 1684"
              }
            </div>
          </div>

          <div className={(slide > 5) ? "person dead" : "person"} id="petr-i">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/4.png`} width="58" />
            </div>
            <div className="name">Петр I</div>
            <div className="year">
              { 
                (slide > 5) ?
                "1672 - 1725" :
                "р. 1672"  
              }
            </div>
          </div>

          <div className={(slide > 9) ? "person dead" : "person"} id="evdokia">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/5.png`} width="58" />
            </div>
            <div className="name">Евдокия (Лопухина)</div>
            <div className="year">
             { 
                (slide > 9) ?
                "1669 - 1731" :
                "р. 1669"  
              }
            </div>
          </div>


          {/* row 2 */}
          <div className={(slide > 14) ? "person dead" : "person"} id="klmsh">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/6.png`} width="58" />
            </div>

            <div className="name">Карл Леопольд Мекленбург-Шверинский</div>
            <div className="year">
             { 
                (slide > 14) ?
                "1678 - 1747" :
                "р. 1678"  
              }
            </div>
          </div>

          <div className={(slide > 9) ? "person dead" : "person"} id="ekaterina-i">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/7.png`} width="58" />
            </div>

            <div className="name">Екатерина Иоанновна</div>
            <div className="year">
              { 
                (slide > 9) ?
                "1691 - 1733" :
                "р. 1691"
              }
            </div>
          </div>

          <div className={(slide > 10) ? "person dead" : "person"} id="anna-i">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/8.png`} width="58" />
            </div>

            <div className="name">Анна Иоанновна</div>
            <div className="year">
              { 
                (slide > 10) ?
                "1693 - 1740" :
                "р. 1693"
              }
            </div>
          </div>

          <div className={(slide > 1) ? "person dead" : "person"} id="fridrikh">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/9.png`} width="58" />
            </div>
            <div className="name">Фридрих Вильгельм Курдлянский</div>
            <div className="year">
              { 
                (slide > 1) ?
                "1692 - 1711" :
                "р. 1692"
              }
            </div>
          </div>

          {/* row 3 */}
          <div className={(slide > 13) ? "person dead" : "person"} id="chirstian">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/10.png`} width="58" />
            </div>
            <div className="name">Христиан Август Ангальт-Цербстский</div>
            <div className="year">
              { 
                (slide > 13) ?
                "1690 - 1747" :
                "р. 1690"
              }
            </div>
          </div>

          <div className={(slide > 14) ? "person dead" : "person"} id="ioganna">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/11.png`} width="58" />
            </div>
            <div className="name">Иоганна Елизавета Гольштейн-Готторпская</div>
            <div className="year">
              { 
                (slide > 14) ?
                "1712 - 1760" :
                "р. 1712"
              }
            </div>
          </div>

          <div className="person dead" id="karl-a">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/12.png`} width="58" />
            </div>
            <div className="name">Карл Август Гольштейн-Готторпский</div>
            <div className="year">1706 - 1727</div>
          </div>

          <div className={(slide > 14) ? "person dead" : "person"} id="elizabeth-p">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/13.png`} width="58" />
            </div>

            <div className="name">Елизавета Петровна</div>
            <div className="year">
              { 
                (slide > 14) ?
                "1709 - 1761" :
                "р. 1709"
              }
            </div>
          </div>

          <div className={(slide > 7) ? "person dead" : "person"} id="anna-p">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/14.png`} width="58" />
            </div>
            <div className="name">Анна Петровна</div>
            <div className="year">
              { 
                (slide > 7) ?
                "1708 - 1728" :
                "р. 1708"
              }
            </div>
          </div>

          <div className={(slide > 9) ? "person dead" : "person"} id="karl-f">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/15.png`} width="58" />
            </div>
            <div className="name">Карл Фридрих Шлезвиг-Гольштейн-Готторпский</div>
            <div className="year">
              { 
                (slide > 9) ?
                "1700 - 1739" :
                "р. 1700"
              }
            </div>
          </div>

          <div className={(slide > 4) ? "person dead" : "person"} id="alexey-p">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/16.png`} width="58" />
            </div>

            <div className="name">Алексей Петрович</div>
            <div className="year">
              { 
                (slide > 4) ?
                "1690 - 1718" :
                "р. 1690"
              }
            </div>
          </div>

          <div className={(slide > 2) ? "person dead" : "person"} id="sks">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/17.png`} width="58" />
            </div>
            <div className="name">Шарлотта Кристина София</div>
            <div className="year">
              { 
                (slide > 2) ?
                "1694 - 1715" :
                "р. 1694"
              }
            </div>
          </div>

          {/* row 4 */}
          <div className={(slide > 13) ? "person dead" : "person"} id="anna-l">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/18.png`} width="58" />
            </div>
            <div className="name">
              Анна <span>Леопольдовна</span>
            </div>
            <div className="year">
              { 
                (slide > 13) ?
                "1718 - 1746" :
                "р. 1718"
              }
            </div>
          </div>

          <div className={(slide > 16) ? "person dead" : "person"} id="anton">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/19.png`} width="58" />
            </div>

            {/*
              <div className="name">Антон Ульрих Брауншвейгский</div>
            */}
            <div className="name">Антон Ульрих</div>
            <div className="year">
              { 
                (slide > 16) ?
                "1714 - 1774" :
                "р. 1714"
              }
            </div>
          </div>

          <div className={(slide > 16) ? "person dead" : "person"} id="ekaterina-ii">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/20.png`} width="58" />
            </div>
            <div className="name">
              {
                (slide > 15) ?
                "Екатерина II" :
                "София Августа Фредерика Ангальт-Цербстская"
              }
            </div>
            <div className="year">
              { 
                (slide > 16) ?
                "1729 - 1796" :
                "р. 1729"
              }
            </div>
          </div>

          <div className={(slide > 15) ? "person dead" : "person"} id="petr-iii">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/21.png`} width="58" />
            </div>
            <div className="name">
              {
                (slide > 14) ?
                "Петр III" :
                "Карл Петер Ульрих"
              }
            </div>

            <div className="year">
              { 
                (slide > 15) ?
                "1728 - 1762" :
                "р. 1728"
              }
            </div>
          </div>

          <div className={(slide > 8) ? "person dead" : "person"} id="petr-ii">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/22.png`} width="58" />
            </div>
            <div className="name">
              { 
                (slide > 6) ?
                "Петр II" :
                "Петр Алексеевич"
              }
            </div>
            <div className="year">
              { 
                (slide > 8) ?
                "1715 - 1730" :
                "р. 1715"
              }
            </div>
          </div>

          {/* row 5 */}

          <div className={(slide > 16) ? "person dead" : "person"} id="ivan-vi">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/23.png`} width="58" />
            </div>
            <div className="name">Иван VI</div>
            <div className="year">
              { 
                (slide > 16) ?
                "1740 - 1764" :
                "р. 1740"
              }
            </div>
          </div>

          <div className="person" id="pavel-i">
            <div className="photo">
              <img src={`${assetsPath}/assets/Dynasty/24.png`} width="58" />
            </div>
            <div className="name">
              { 
                (slide > 16) ?
                "Павел I" :
                "Павел"
              }
            </div>
            <div className="year">
              { 
                (slide > 0) ?
                "р. 1754" :
                "р. 1754"
              }
            </div>
          </div>

        </div>

        {
          timelineClassName === 'timeline visible' ?
          <div style={{height: 67, marginTop: 200}} /> :
          null
        }

        <div className={timelineClassName}>

          <div className="timeline__inner">
            <button onClick={this.prevSlide.bind(this)} className={prevButtonClass}>&larr;</button>
            <button onClick={this.nextSlide.bind(this)} className={nextButtonClass}>&rarr;</button>

            <ul>
              <li>

              </li>
              
              <li>
                <span />
                <p>1710</p>
                <span />
              </li>
              
              <li>
                <span />
                <p>1720</p>
                <span />
              </li>
              
              <li>
                <span />
                <p>1730</p>
                <span />
              </li>
              
              <li>
                <span />
                <p>1750</p>
                <span />
              </li>
              
              <li>
                <span />
                <p>1770</p>
                <span />
              </li>
            </ul>

            <div className="timeline__current" style={{width: timelineCurrentWidth}}>
              <div className="timeline__year">
                <div>{timelineCurrentYear}</div>
              </div>
            </div>

          </div>

        </div>

        <div className="dynasty__end" />

        {/*
          <br />
          slide: {slide}
        */}

      </div>
    )
  }
}


export default class extends React.Component {

  componentDidMount() {
    setTimeout(()=>{
        $('html, body').animate({
          scrollTop: 0
        }, 0);
      }, 10)
  }

  render() {

    const { answers } = this.props;

    return (
      <div className="result">

        <header>
          <p className="subheader">Петр I. Геополитика</p>
          <header>Как отдать всех дочерей в рабство и не облажаться</header>
        </header>

        <Share answers={answers} />

        <Dynasty />

      </div>
    )
  }

}