const questions = window.questions;
const results = window.results;

const slides = [
  {
    'year': 1710
  },
  {
    'year': 1710
  },
  {
    'year': 1711
  },
  {
    'year': 1715
  },
  {
    'year': 1716
  },
  {
    'year': 1718
  },
  {
    'year': 1725
  },
  {
    'year': 1727
  },
  {
    'year': 1728
  },
  {
    'year': 1730
  },
  // 11
  {
    'year': 1739
  },
  {
    'year': 1740
  },
  {
    'year': 1742
  },
  {
    'year': 1745
  },
  // {
  //   'year': 1746
  // },
  {
    'year': 1754
  },
  {
    'year': 1761
  },
  {
    'year': 1762
  },
  {
    'year': 1764
  },
  {
    'year': 1796
  }
]

export {
  questions,
  slides,
  results
};

export default questions;