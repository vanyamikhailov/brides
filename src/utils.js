import $ from 'jquery';


export function shareToFacebook(url) {
  const popupWidth = 640;
  const popupHeight = 325;
  const popupLeft = ($(window).width() / 2) - (popupWidth / 2);
  const popupTop = ($(window).height() / 2) - (popupHeight / 2);

  const pageUrl = encodeURIComponent(url);
  const link = `http://www.facebook.com/sharer.php?u=${pageUrl}`;
  const popupOptions = `width=${popupWidth},height=${popupHeight},left=${popupLeft},top=${popupTop}`;

  window.open(link, '', popupOptions);
}

export function shareToTwitter(url, title) {
  // title = he.decode(title);

  const popupHeight = 262;
  const popupWidth = 586;
  const popupLeft = ($(window).width() / 2) - (popupWidth / 2);
  const popupTop = ($(window).height() / 2) - (popupHeight / 2);

  const msg = encodeURIComponent(title);
  const pageUrl = encodeURIComponent(url);

  const link = `http://twitter.com/intent/tweet?text=${msg}&url=${pageUrl}`;

  const popupOptions = `width=${popupWidth},height=${popupHeight},left=${popupLeft},top=${popupTop}`;

  window.open(link, '', popupOptions);
}

export function shareToVK(url) {
  const popupWidth = 640;
  const popupHeight = 325;
  const popupLeft = ($(window).width() / 2) - (popupWidth / 2);
  const popupTop = ($(window).height() / 2) - (popupHeight / 2);

  const pageUrl = encodeURIComponent(url);
  const link = `http://vk.com/share.php?url=${pageUrl}`;
  const popupOptions = `width=${popupWidth},height=${popupHeight},left=${popupLeft},top=${popupTop}`;

  window.open(link, '', popupOptions);
}


export function shareToOK(url) {
  const popupWidth = 640;
  const popupHeight = 325;
  const popupLeft = ($(window).width() / 2) - (popupWidth / 2);
  const popupTop = ($(window).height() / 2) - (popupHeight / 2);

  const pageUrl = encodeURIComponent(url);
  const link = `http://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=${pageUrl}`;
  const popupOptions = `width=${popupWidth},height=${popupHeight},left=${popupLeft},top=${popupTop}`;

  window.open(link, '', popupOptions);
}