import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import Portal from 'react-portal';

import data from './data';
import { IntroScreen, QuizScreen, AnswerScreen, OptionScreen, ResultScreen } from './containers';
 
require('./styles/app.styl')


class App extends Component {

  state = {
    question: null,
    selectedOption: null,
    lastAnswerIsCorrect: null,
    result: false
  }

  answers = []

  selectOption(selectedOption) {
    this.setState({ selectedOption })
  }

  removeOption() {
    this.setState({ selectedOption: null })
  }

  start() {
    this.setState({
      question: 0
    }, () => {
      $('html, body').animate({
        scrollTop: $("#brides-game").offset().top - 100
      }, 0);
    })
  }

  prevGroom() {
    const { question, selectedOption } = this.state;
    const options = data[question]['options']; 
    if (selectedOption - 1 >= 0) {
      this.selectOption.bind(this, selectedOption - 1)()
    } else {
      this.selectOption.bind(this, 2)()
    }
  }

  nextGroom() {
    const { question, selectedOption } = this.state;

    if (selectedOption + 1 <= options.length - 1) {
      this.selectOption.bind(this, selectedOption + 1)()
    } else {
      this.selectOption.bind(this, 0)()
    }
  }


  checkAnswer() {
    const { question, selectedOption } = this.state;

    if (data[question].correctAnswer === selectedOption ) {
      this.answers.push(true)
      this.setState({ lastAnswerIsCorrect: true })
    } else {
      this.answers.push(false)
      this.setState({ lastAnswerIsCorrect: false })
    }

    document.getElementById('brides-portal').scrollTop = 0;
  }

  componentDidMount() {
    
  }

  componentDidUpdate(prevProps, prevState) {
    const { selectedOption, lastAnswerIsCorrect } = this.state;

    if (selectedOption !== null || lastAnswerIsCorrect !== null) {
      document.body.style.overflow = 'hidden';
    }

    if (selectedOption === null && lastAnswerIsCorrect === null) {
      document.body.style.overflow = 'auto';
    }

  }

  render() {
    const { question, selectedOption, lastAnswerIsCorrect, result } = this.state;

    if (result === true) {
      return (
        <div className="brides">
          <ResultScreen answers={this.answers} />
        </div>
      )
    }

    return (
      <div className="brides">
      {
        question === null ?
        <IntroScreen 
          startButtonHandler={this.start.bind(this)} 
          dynastyHandler={()=>{
            this.answers = [false, true, false, true, true]
            this.setState({
              result: true
            })
          }}
        /> :
        <QuizScreen 
          question={question} 
          pinClickHandler={(selectedOption) => {
            this.selectOption.bind(this)(selectedOption, true)
          }} 
        />
      }

      {
        selectedOption !== null ?
          (lastAnswerIsCorrect !== null) ?
            <Portal isOpened={true}>
                <div className="brides-portal" id="brides-portal">
                  <div className="mask"  />
                  <AnswerScreen
                    question={question}
                    option={selectedOption}
                    correct={lastAnswerIsCorrect}
                    nextButtonHandler={()=>{
                      if (question + 1 < data.length) {
                        console.log('question + 1', question + 1)
                        this.setState({
                          question: question + 1,
                          selectedOption: null,
                          lastAnswerIsCorrect: null
                        }, () => {
                          $('html, body').animate({
                            scrollTop: $(".bio").offset().top - 150
                          }, 300);
                        })
                      } else {
                        this.setState({
                          result: true,
                          selectedOption: null,
                          lastAnswerIsCorrect: null
                        })
                      }
                    }}
                  />
                </div>
              </Portal> :
              <Portal closeOnEsc isOpened={true}>
                <div className="brides-portal" id="brides-portal">
                  <div className="mask" onClick={this.removeOption.bind(this)} />
                  <OptionScreen 
                    question={question}
                    selectedOption={selectedOption} 
                    prevButtonHandler={this.prevGroom.bind(this)}
                    nextButtonHandler={this.prevGroom.bind(this)}
                    closeButtonHandler={this.removeOption.bind(this)}
                    checkAnswerButtonHandler={this.checkAnswer.bind(this)}
                  />
                </div>
              </Portal>
            : null
      }


      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('brides-game'));
