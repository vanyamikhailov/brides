var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var CleanPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './src/app.js',
    devtool: 'source-map',
    cache: true,
    debug: false,
    output: {
        path: './dist/',
        filename: 'brides.[hash].js',
        // publicPath: 'http://cdn-s-static.arzamas.academy/x/334-school-w9gxEU0N22MfiGAcrMoZs7TAa3/1137/dist/'
        publicPath: '/'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        },
        { test: /\.styl$/, loader: ExtractTextPlugin.extract(['css-loader', 'stylus-loader']) },
        ]
    },
    plugins: [
        new CleanPlugin(['./dist'], { root: path.resolve(__dirname) }),
        new ExtractTextPlugin("brides.[hash].css"),
        new HtmlWebpackPlugin({
            inject: 'body',
            template: './src/index.html'
        }),
        CopyWebpackPlugin([
            { from: './src/assets', to: './assets'},
        ])
    ]
};